#!/usr/bin/env bash

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"; done # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
DIR_PARENT="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
#DIR_PARENT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )" # alternative path one-liner, does not follow symlinks

[ ! -d $DIR_PARENT'/logs' ] && mkdir $DIR_PARENT'/logs'
[ ! -f $DIR_PARENT'/.nodes' ] && touch $DIR_PARENT'/.nodes'

DIR_INCLUDE=$DIR_PARENT'/incl'
FILE_EXTEND=$DIR_PARENT'/.extends'
FILE_NODE=$DIR_PARENT'/.nodes'
FILE_LOG_SYS=$DIR_PARENT'/SYSLOG'
DIR_TEMP=$DIR_PARENT'/.temp_dir'

NODES_ARRAY=(`cat $FILE_NODE`)
NODES_ARRAY_NO=${#NODES_ARRAY[@]}
NODES_FAILED=()

EXTEND_ARRAY=(`cat $FILE_EXTEND`)

for srce in $DIR_INCLUDE/*; do . $srce; done

update_nodes_array () {
  NODES_ARRAY=(`cat $FILE_NODE`)
  if [ $NODES_ARRAY_NO -ne ${#NODES_ARRAY[@]} ]; then
    echo "$(date +"%Y-%m-%d %H:%M:%S") [i] Nodes number have changed by $(( ${#NODES_ARRAY[@]} - $NODES_ARRAY_NO ))" | tee -a "$FILE_LOG_SYS"
    NODES_ARRAY_NO=${#NODES_ARRAY[@]}; fi; }

query_node () {
  snmpwalk -v2c -c public $1 'NET-SNMP-EXTEND-MIB::nsExtendOutLine'  1>$DIR_TEMP/$1 2>/dev/null
  if [[ $? -ne 0 ]]; then
    if ! array-entry-lookup $1 NODES_FAILED; then
      echo "$(date +"%Y-%m-%d %H:%M:%S") [!] $1 unreachable" | tee -a "$FILE_LOG_SYS"
      NODES_FAILED+=($1); fi
  else
    if array-entry-lookup $1 NODES_FAILED; then
      echo "$(date +"%Y-%m-%d %H:%M:%S") [i] $1 reachable" | tee -a "$FILE_LOG_SYS"
      index=0
      for entry in ${NODES_FAILED[@]}; do
            if [[ "$entry" == "$1" ]]; then unset NODES_FAILED["$index"]; NODES_FAILED=( "${NODES_FAILED[@]}" ); break; fi
            let index++; done; unset index; fi
    mkdir -p $DIR_PARENT/logs/$node/$(date +"%Y-%m-%d")
    for extension in ${EXTEND_ARRAY[@]}; do
      if result=$(cat $DIR_TEMP/$1 | grep $extension); then
        echo "$(date +"%Y-%m-%d %H:%M:%S") [i] $1 $extension ${result//*: /}" | tee -a $DIR_PARENT'/logs/'$1'/'$(date +"%Y-%m-%d")/$extension; fi; done; fi
}

echo "$(date +"%Y-%m-%d %H:%M:%S") [i] Starting the background service" >> "$FILE_LOG_SYS"
echo "$(date +"%Y-%m-%d %H:%M:%S") [i] Querying $NODES_ARRAY_NO node(s)" >> "$FILE_LOG_SYS"

finish () {
  rm -r $DIR_TEMP &>/dev/null
  echo "$(date +"%Y-%m-%d %H:%M:%S") [i] Stopping the background service" >> "$FILE_LOG_SYS";
}

trap finish EXIT

[ ! -d $DIR_TEMP ] && mkdir $DIR_TEMP

while true; do
  for node in ${NODES_ARRAY[@]}; do
    query_node "$node"; done
  sleep 1m
  update_nodes_array; done

return 0 2>/dev/null
exit 0
