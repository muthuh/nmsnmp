#!/usr/bin/env bash
OUT=$(cat /proc/loadavg)
OUT="${OUT:0:14}"
OUT="${OUT//0./}"
echo $OUT
