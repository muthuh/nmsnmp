#!/usr/bin/env bash

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"; done # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
DIR_PARENT="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
#DIR_PARENT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )" # alternative path one-liner, does not follow symlinks

[ ! -d $DIR_PARENT'/logs' ] && mkdir $DIR_PARENT'/logs'
[ ! -f $DIR_PARENT'/.nodes' ] && touch $DIR_PARENT'/.nodes'

DIR_INCLUDE=$DIR_PARENT'/incl'
DIR_LOGS=$DIR_PARENT'/logs'
FILE_EXTEND=$DIR_PARENT'/.extends'
FILE_NODES=$DIR_PARENT'/.nodes'
FILE_TEMP=$DIR_PARENT'/.temp_file'
FILE_LOG_SYSTEM=$DIR_PARENT'/SYSLOG'

EXTEND_ARRAY=(`cat $FILE_EXTEND`)

NODES_ARRAY=(`cat $FILE_NODES`)
NODES_ARRAY_NO=${#NODES_ARRAY[@]}

for srce in $DIR_INCLUDE/*; do . $srce; done

finish () {
	echo-separate "\u203e"
	rm $FILE_TEMP &>/dev/null; }

update_nodes_array () {
  NODES_ARRAY=(`cat $FILE_NODES`)
  NODES_ARRAY_NO=${#NODES_ARRAY[@]}; }

node_add () {
  read-input " Node IP "
  if [[ $input =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
    if array-entry-lookup $input NODES_ARRAY; then
      echo " already in the list"
		else
      echo $input >> $FILE_NODES
			update_nodes_array
			echo "$(date +"%Y-%m-%d %H:%M:%S") [i] Node $input added" | tee -a "$FILE_LOG_SYSTEM"; fi
  else echo " invalid"; fi; }

node_a_graph () {
  DATE=$1
    if array-pick-from_nodes; then
      dir_plot=$DIR_LOGS'/'$input'/'$DATE
      this_extend_array=(`ls  $dir_plot`)
      echo-separate "_"
      read -n 1 -p " Graph(s) should pop-open, hit anything (prefereably a key) " && echo
      for extension in ${this_extend_array[@]}; do
        plotter $input $extension; done
    else
    echo " invalid"; fi; }

plotter () {
  read -r extract_params<"$dir_plot/$2"
  params_array=(${extract_params/*$2/})
  cmnd=''
  n=6
  for param in ${params_array[@]}; do
    cmnd=$cmnd" \"$dir_plot/$2\" using 2:$n with lines title \"${2//_/ } ($(( n - 5 )))\","
    let n++; done
  (gnuplot --persist -e "set xdata time" -e "set timefmt \"%H:%M:%S\"" -e "set format x \"%H:%M\"" -e "set xlabel \"$DATE\"" -e "set ylabel \"${2//_/ }\"" -e "set title \"$1\"" -e "plot $cmnd; pause -1" &>/dev/null) &
}

array-pick-from_nodes () {
  echo-array-numbered NODES_ARRAY
  read-input " Select a node"
  if array-entry-lookup "$input" NODES_ARRAY; then
    return 0
  elif [[ ! -n ${input//[0-9]/} && "$input" > 0 && "$input" -lt $NODES_ARRAY_NO+1 ]] 2>/dev/null; then
    input=$(sed -n "${input}p" $FILE_NODES)
    return 0
  else
    return 1; fi; }

node_remove () {
  echo-array-numbered NODES_ARRAY
  read-input " Select node to remove"
  if array-entry-lookup "$input" NODES_ARRAY; then
    line_no=$(grep -n "$input" $FILE_NODES | cut -f1 -d:)
    sed -e "${line_no}d" $FILE_NODES > $FILE_TEMP; cp $FILE_TEMP $FILE_NODES; rm $FILE_TEMP
		update_nodes_array
		echo "$(date +"%Y-%m-%d %H:%M:%S") [i] Node $input removed" | tee -a "$FILE_LOG_SYSTEM"
  elif [[ ! -n ${input//[0-9]/} && "$input" > 0 && "$input" -lt $NODES_ARRAY_NO+1 ]] 2>/dev/null; then
		ip=$(sed -n "${input}p" $FILE_NODES)
    sed -e "${input}d" $FILE_NODES > $FILE_TEMP; cp $FILE_TEMP $FILE_NODES; rm $FILE_TEMP
		update_nodes_array
		echo "$(date +"%Y-%m-%d %H:%M:%S") [i] Node $ip removed" | tee -a "$FILE_LOG_SYSTEM"
  else
    echo " invalid"; fi; }

nodes_print () {
  echo-array-numbered NODES_ARRAY; }

menu_nodes () {
  while true; do
    read-cr "\033[1m:: Nodes\033[0m\n 1) List\n 2) Add\n 3) Remove\n 4) BACK"
    case $input in
      1) echo-separate "_"; nodes_print;;
      2) echo-separate "_"; node_add;;
      3) echo-separate "_"; node_remove;;
      4) break;;
      *) :;; esac
    echo-separate "_"; done; }

menu_glance_last_ten () {
	read-cr " 1) Entries 2) Warnings 3) Info's"
	case $input in
		1) echo-separate "_"; cat $FILE_LOG_SYSTEM |  tail;;
		2) echo-separate "_"; cat $FILE_LOG_SYSTEM |  grep [!] | tail;;
		3) echo-separate "_"; cat $FILE_LOG_SYSTEM |  grep [i] | tail;;
		*) echo " invalid";; esac; }

menu_service () {
	while true; do
		read-cr "\033[1m:: Background service\033[0m\n 1) Current state\n 2) Toggle state\n 3) Enable status\n 4) Toggle enable status\n 5) BACK"
		case $input in
			1) echo-separate "_"; if systemctl --user status nmsnmp &>/dev/null; then echo " running"; else echo " *not* running"; fi;;
			2) echo-separate "_"; if systemctl --user status nmsnmp &>/dev/null; then systemctl --user stop nmsnmp &>/dev/null && echo " halt"; else systemctl --user start nmsnmp &>/dev/null && echo " start"; fi;;
			3) echo-separate "_"; echo -n " "; systemctl --user is-enabled nmsnmp;;
			4) echo-separate "_"; if systemctl --user is-enabled nmsnmp &>/dev/null; then systemctl --user disable nmsnmp &>/dev/null; echo "$(date +"%Y-%m-%d %H:%M:%S") [i] Background service disabled" >> $FILE_LOG_SYSTEM; echo " disable"; else systemctl --user enable nmsnmp &>/dev/null; echo "$(date +"%Y-%m-%d %H:%M:%S") [i] Background service enabled" >> $FILE_LOG_SYSTEM; echo " enable"; fi;;
			5) break;;
			*) :;; esac
		echo-separate "_"; done; }

menu_syslog () {
  while true; do
    read-cr "\033[1m:: Syslog\033[0m\n 1) Today's entries\n 2) Today's warnings\n 3) Today's info's\n 4) Last ten..\n 5) All\n 6) BACK"
    case $input in
      1) echo-separate "_"; cat $FILE_LOG_SYSTEM | grep $(date +"%Y-%m-%d");;
      2) echo-separate "_"; cat $FILE_LOG_SYSTEM | grep $(date +"%Y-%m-%d") | grep [!];;
      3) echo-separate "_"; cat $FILE_LOG_SYSTEM | grep $(date +"%Y-%m-%d") | grep [i];;
      4) echo-separate "_"; menu_glance_last_ten;;
      5) echo-separate "_"; cat $FILE_LOG_SYSTEM;;
      6) break;;
      *) :;; esac
    echo-separate "_"; done; }

menu_current_readings () {
	read-cr " Query..\n 1) Single node 2) All"
	case $input in
		1) echo-separate "_"; query_node;;
		2) echo-separate "_"; query_all;;
		*) echo " invalid";; esac; }

menu_graphs () {
	while true; do
		read-cr "\033[1m:: Graphs\033[0m\n 1) Today's\n 2) By date\n 3) BACK"
		case $input in
          1) echo-separate "_"; node_a_graph $(date +"%Y-%m-%d");;
          # 1) echo-separate "_"; dir_plot=$DIR_LOGS'/'$(date +"%Y-%m-%d"); echo -en " <CR> when done with the graph"; plotter ${NODES_ARRAY[@]};;
          2) echo-separate "_"; date_picker;;
          3) break;;
          *) echo " invalid";; esac
		echo-separate "_"; done; }

query_all () {
	echo -e " Querying hosts;"
	for node in ${NODES_ARRAY[@]}; do
    (snmpwalk -v2c -c public $node 'NET-SNMP-EXTEND-MIB::nsExtendOutLine' 1>$FILE_TEMP 2>/dev/null) &
    pid=$!
    echo -en " \033[1m$node\033[0m\t"
    spinner $pid &
    wait $pid
    if [[ $? -ne 0 ]]; then
      echo "unreachable"
    else
      echo -e "\n$(cat $FILE_TEMP)"; fi; done; }

query_node () {
  echo-array-numbered NODES_ARRAY
  read-input " Select node or manual IP input"
  if array-entry-lookup "$input" NODES_ARRAY || [[ $input =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
    (snmpwalk -v2c -c public $input 'NET-SNMP-EXTEND-MIB::nsExtendOutLine' 1>$FILE_TEMP 2>/dev/null) &
    await_pid
    if [[ $? -ne 0 ]]; then
      echo "unreachable"
    else
      echo -e "\n$(cat $FILE_TEMP)"; fi
  elif [[ ! -n ${input//[0-9]/} && "$input" > 0 && "$input" -lt $NODES_ARRAY_NO+1 ]] 2>/dev/null; then
    input=$(sed "${input}q;d" $FILE_NODES)
    (snmpwalk -v2c -c public $input 'NET-SNMP-EXTEND-MIB::nsExtendOutLine' 1>$FILE_TEMP 2>/dev/null) &
    await_pid
    if [[ $? -ne 0 ]]; then
      echo "unreachable"
    else
      echo -e "\n$(cat $FILE_TEMP)"; fi
  else
    echo " invalid"; fi; }

date_picker () {
	cd $DIR_LOGS
	shopt -s nullglob
	LOG_DATES=(*)
	shopt -u nullglob
	echo-array-numbered LOG_DATES
	read-input " Select date"
	if array-entry-lookup "$input" LOG_DATES; then
		echo " not implemented"
	elif [[ ! -n ${input//[0-9]/} && "$input" > 0 ]] 2>/dev/null; then
		((input-=1))
		DATE=${LOG_DATES[$input]}
		dir_plot=$DIR_LOGS'/'$DATE
		echo -en " <CR> when finished with the graph";
		plotter ${NODES_ARRAY[@]}
	else
		echo " invalid"; fi; }

trap finish EXIT
echo -en "\ec" # clean the screen

if [[ "$1" == "-b" ]]; then ($DIR_PARENT/background.sh) & wait $!
elif [[ "$#" == 0 ]]; then
	while true; do
		echo-separate '_'
	  read-cr "\033[1m:: NMSNMP main\033[0m\n 1) Query..\n 2) Node menu\n 3) Graphs\n 4) Inspect SYSLOG\n 5) Manage background service\n 0) EXIT"
	  case $input in
			0) exit 0;; #0) screen -X -S nmsnmp detach;;
	    1) echo-separate "_"; menu_current_readings;;
	    2) echo-separate "_"; menu_nodes;;
        3) echo-separate "_"; menu_graphs;;
		4) echo-separate "_"; menu_syslog;;
		5) echo-separate "_"; menu_service;;
	    *) :;; esac; done; fi

return 0 2>/dev/null
exit 0
