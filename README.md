# NMSNMP

Network Management SNMP

## Simple SNMP manager to retrieve readings from the on-board CPU temperature sensor from remote Linux-based host.

- **WORD OF WARNING** some adjustments might be necessary, tested only on my own local system
- **TODO** user manual, implement changes that would allow for execution on any modern Linux-based desktop
- **PREREQUISITES** GNU screen, gnuplot; maybe couple more
- **USAGE** `./nmsnmp.sh`
