#!/usr/bin/env bash

DIR_PARENT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

mkdir -p ~/.config/systemd/user
cp $DIR_PARENT/util/nmsnmp.service ~/.config/systemd/user/

ln -sfT $DIR_PARENT/nmsnmp.sh ~/.local/bin/nmsnmp

return 0 2>/dev/null
exit 0
